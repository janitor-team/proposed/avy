avy (0.5.0-2) unstable; urgency=medium

  * Rebuild against newer dh_elpa
  * Add upstream metadata
  * d/control: Migrate to debhelper-compat 13
  * d/control: Declare Standards-Version 4.5.1 (no changes needed)
  * d/control: Drop emacs25 from Enhances
  * d/copyright: Bump copyright years
  * d/rules: Ship changelog in org-mode format

 -- Lev Lamberov <dogsleg@debian.org>  Tue, 08 Dec 2020 10:54:18 +0500

avy (0.5.0-1) unstable; urgency=medium

  * New upstream version 0.5.0
  * Migrate to debhelper 12 without d/compat
  * Drop obsolete README.Debian
  * d/control: Add Rules-Requires-Root: no
  * d/control: Declare Standards-Version 4.4.0
  * d/control: Drop Built-Using field
  * d/control: Drop emacs from Depends
  * d/control: Update Maintainer (to Debian Emacsen team)
  * d/copyright: Bump copyright years
  * d/rules: Do not pass --parallel to dh

 -- Lev Lamberov <dogsleg@debian.org>  Mon, 08 Jul 2019 18:46:16 +0500

avy (0.4.0+git20190328.85b5d574-1) unstable; urgency=medium

  * New upstream snapshot, incorporating bug fixes

 -- Lev Lamberov <dogsleg@debian.org>  Fri, 29 Mar 2019 18:26:09 +0500

avy (0.4.0+git20180322.2d613eb0-1) unstable; urgency=medium

  * Team upload.
  * New upstream snapshot, incorporating many bug fixes.
    I have been using this snapshot for some months, and consider it
    suitable for a Debian stable release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 06 Jun 2018 12:21:10 +0100

avy (0.4.0-3) unstable; urgency=medium

  * Team upload.
  * Rebuild with dh-elpa 1.13 to fix byte-compilation with unversioned
    emacs

 -- David Bremner <bremner@debian.org>  Fri, 01 Jun 2018 20:36:00 -0300

avy (0.4.0-2) unstable; urgency=medium

  * Do not Build-Depend on emacs-nox
  * Enhance emacs25, not emacs24

 -- Lev Lamberov <dogsleg@debian.org>  Tue, 27 Dec 2016 22:52:39 +0500

avy (0.4.0-1) unstable; urgency=medium

  * Initial release (Closes: #839258)

 -- Lev Lamberov <dogsleg@debian.org>  Sat, 1 Oct 2016 00:36:30 +0500
